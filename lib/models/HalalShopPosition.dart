import 'package:equatable/equatable.dart';


// ignore: must_be_immutable
class HalalShopPosition extends Equatable{

  String markerId;
  double latitude;
  double longitude;
  String title;


  HalalShopPosition({
    this.markerId,
    this.latitude,
    this.longitude,
    this.title
  });

  @override
  List<Object> get props => [markerId, latitude, longitude, title];

  @override
  bool get stringify => false;

}