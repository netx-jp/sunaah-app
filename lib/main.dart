import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sunaah/pages/DashboardPage.dart';
import 'package:sunaah/pages/ErrorPage.dart';
import 'package:sunaah/pages/OverviewPage.dart';
import 'package:sunaah/pages/WelcomePage.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
    statusBarColor: Colors.yellow.shade300, // Color for Android
    statusBarBrightness: Brightness.dark // Dark for IOS.
  ));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sunaah',
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        '/': (context) => WelcomePage(),
        '/error': (context) => ErrorPage(),
        '/overview': (context) => OverviewPage(data: null),
        '/dashboard': (context) => DashboardPage(data: null)
      }
    );
  }
}
