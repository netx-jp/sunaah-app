import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:sunaah/models/HalalShopPosition.dart';

class HalalShopFinderComponent extends StatefulWidget {
  final data;

  HalalShopFinderComponent({Key key, this.data}) : super(key: key);

  @override
  HalalShopFinderComponentState createState() => HalalShopFinderComponentState();
}

class HalalShopFinderComponentState extends State<HalalShopFinderComponent> {
  Completer<GoogleMapController> completer = Completer();

  List<HalalShopPosition> halalShopPositions = [
    HalalShopPosition(
      title: "Dhaka",
      latitude: 40.738380,
      longitude: -73.988426,
      markerId: "1"
    ),
    HalalShopPosition(
      title: "Chittagong",
      latitude: 40.761421,
      longitude: -73.981667,
      markerId: "2"
    ),
    HalalShopPosition(
      title: "Khulna",
      latitude: 40.732128,
      longitude: -73.999619,
      markerId: "3"
    ),
    HalalShopPosition(
      title: "Rajshahi",
      latitude: 40.742451,
      longitude: -74.005959,
      markerId: "4"
    ),
    HalalShopPosition(
      title: "Rangpur",
      latitude: 40.729640,
      longitude: -73.983510,
      markerId: "5"
    )
  ];

  CameraPosition initialCameraPosition;
  Set<Marker> markers = {};

  @override
  void initState() {
    super.initState();
    initialCameraPosition = CameraPosition(
      target: LatLng(
        halalShopPositions[0].latitude,
        halalShopPositions[0].longitude
      ),
      zoom: 20.0,
    );
    markers.clear();
    halalShopPositions.forEach((element) {
      markers.add(
        Marker(
          markerId: MarkerId(element.markerId),
          position: LatLng(element.latitude, element.longitude),
          infoWindow: InfoWindow(title: element.title),
          icon: BitmapDescriptor.defaultMarkerWithHue(
            BitmapDescriptor.hueViolet,
          ),
        )
      );
    });
  }

  TextEditingController typeAheadController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      body: Container(
        color: Colors.yellow,
        margin: EdgeInsets.only(bottom: 60),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.amber.shade100,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20)
            )
          ),
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: 10),
                  Text(
                      "Search Halal Shop",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.w500
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    height: 40,
                    child: TypeAheadField(
                      textFieldConfiguration: TextFieldConfiguration(
                        controller: typeAheadController,
                        autofocus: false,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(10.0),
                          hintText: "SEARCH",
                          alignLabelWithHint: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(25.0)),
                            borderSide: BorderSide(color: Colors.black87),
                          ),
                          suffixIcon: Icon(Icons.search)
                        )
                      ),
                      suggestionsCallback: (pattern) {
                        return halalShopPositions;
                      },
                      itemBuilder: (context, halalShopPosition) {
                        return ListTile(
                            leading: Icon(Icons.shopping_cart),
                            title: Text(halalShopPosition.title)
                        );
                      },
                      onSuggestionSelected: (halalShopPosition) {
                        typeAheadController.text = halalShopPosition.title;
                        completer.future.then((c){
                          c.animateCamera(CameraUpdate.newCameraPosition(
                            CameraPosition(
                              target: LatLng(
                                halalShopPosition.latitude,
                                halalShopPosition.longitude,
                              ),
                              zoom: 15.0
                            )));
                        });
                      },
                    ),
                    margin: EdgeInsets.only(left: 20, right: 20),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: 300,
                    height: 250,
                    child: GoogleMap(
                      mapType: MapType.normal,
                      initialCameraPosition: initialCameraPosition,
                      onMapCreated: (GoogleMapController googleMapController) {
                        completer.complete(googleMapController);
                      },
                      markers: markers,
                    ),
                  ),
                  SizedBox(height: 20),
                  InkWell(
                    child: Container(
                      width: 200.0,
                      height: 40.0,
                      decoration: BoxDecoration(
                        color: Colors.blueAccent,
                        borderRadius: new BorderRadius.circular(20.0),
                      ),
                      child: Center(
                          child: Text("Next",
                              style: TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.white
                              )
                          )
                      ),
                    ),
                  ),
                  SizedBox(height: 30),
                  Container(
                    width: screenSize.width,
                    height: 60,
                    margin: EdgeInsets.only(left: 20,right: 20),
                    decoration: BoxDecoration(
                      color: Colors.blueAccent,
                      borderRadius: new BorderRadius.circular(5.0),
                    ),
                    child: Center(
                      child: Text("ADDS BANNER",
                        style: TextStyle(
                          fontSize: 25.0,
                          color: Colors.white
                        )
                      )
                    ),
                  ),
                  SizedBox(height: 30),
                ],
              ),
            ),
          ),
        ),
      )
    );
  }
}