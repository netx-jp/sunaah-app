import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:sunaah/components/HalalShopFinderComponent.dart';
import 'package:sunaah/utilities/CustomSearchDelegate.dart';

class DashboardPage extends StatefulWidget {
  final data;

  DashboardPage({Key key, this.data}) : super(key: key);

  @override
  DashboardPageState createState() => DashboardPageState();
}

class DashboardPageState extends State<DashboardPage> {

  List<Widget> buildScreens(BuildContext context) {
    return [
      HalalShopFinderComponent(data: null),
      Container(
        color: Colors.yellow,
        child: Container(
          decoration: BoxDecoration(
              color: Colors.amber.shade100,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20))),
          child: Center(
            child: Text("2"),
          ),
        ),
      ),
      Container(
        color: Colors.yellow,
        child: Container(
          decoration: BoxDecoration(
              color: Colors.amber.shade100,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20))),
          child: Center(
            child: Text("3"),
          ),
        ),
      ),
      Container(
        color: Colors.yellow,
        child: Container(
          decoration: BoxDecoration(
              color: Colors.amber.shade100,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20))),
          child: Center(
            child: Text("4"),
          ),
        ),
      ),
      Container(
        color: Colors.yellow,
        child: Container(
          decoration: BoxDecoration(
              color: Colors.amber.shade100,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20))),
          child: Center(
            child: Text("5"),
          ),
        ),
      )
    ];
  }

  List<PersistentBottomNavBarItem> navBarItems() {
    return [
      PersistentBottomNavBarItem(
          icon: Icon(Icons.home_outlined),
          title: "HOME",
          activeColorPrimary: Colors.blue,
          inactiveColorPrimary: Colors.black),
      PersistentBottomNavBarItem(
          icon: Icon(Icons.qr_code_scanner),
          title: ("SCAN"),
          activeColorPrimary: Colors.blue,
          inactiveColorPrimary: Colors.black),
      PersistentBottomNavBarItem(
          icon: Icon(Icons.shopping_cart_outlined),
          title: ("SHOP"),
          activeColorPrimary: Colors.blue,
          inactiveColorPrimary: Colors.black,
          activeColorSecondary: Colors.white),
      PersistentBottomNavBarItem(
          icon: Icon(Icons.show_chart),
          title: ("SELL"),
          activeColorPrimary: Colors.blue,
          inactiveColorPrimary: Colors.black),
      PersistentBottomNavBarItem(
          icon: Icon(Icons.menu_sharp),
          title: ("MENU"),
          activeColorPrimary: Colors.blue,
          inactiveColorPrimary: Colors.black),
    ];
  }

  PersistentTabController persistentTabController;
  TextEditingController searchInputCtl = new TextEditingController();
  bool hideNavBar;


  @override
  void initState() {
    persistentTabController = PersistentTabController(initialIndex: 0);
    hideNavBar = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(160),
          child: Container(
            padding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
            color: Colors.yellow,
            height: 170,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        width: 100,
                        height: 100,
                        child: Image.asset('assets/images/logo.png')
                    ),
                    Row(
                      children: [
                        InkWell(
                          child: Badge(
                            position:
                            BadgePosition.topStart(start: 15, top: -20),
                            badgeColor: Colors.blue,
                            child: Icon(Icons.shopping_bag_outlined),
                            badgeContent: Text(
                              "20",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        InkWell(
                          child: Container(
                            height: 45,
                            width: 45,
                            child: CircleAvatar(
                                radius: 20,
                                backgroundImage: AssetImage('assets/images/dummy_user_image.png')
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20))
                  ),
                  padding: EdgeInsets.only(left: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        child: Text(
                          "Search for HALAL product",
                          style: TextStyle(
                              fontSize: 17
                          ),
                        ),
                        onTap: (){
                          showSearch(
                            context: context,
                            delegate: CustomSearchDelegate())
                          ;
                        },
                      ),
                      IconButton(
                        onPressed: () async {
                          String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
                              "#ff6666", "Cancel", false, ScanMode.DEFAULT
                          );
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("bar code = $barcodeScanRes"),
                          ));
                        },
                        icon: Icon(Icons.qr_code_scanner_outlined)
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        body: Container(
          child: PersistentTabView(
            context,
            controller: persistentTabController,
            screens: buildScreens(context),
            items: navBarItems(),
            confineInSafeArea: true,
            backgroundColor: Colors.yellow,
            handleAndroidBackButtonPress: true,
            resizeToAvoidBottomInset: true,
            stateManagement: true,
            navBarHeight:
                MediaQuery.of(context).viewInsets.bottom > 0 ? 0.0 : 60.0,
            hideNavigationBarWhenKeyboardShows: true,
            margin: EdgeInsets.all(0.0),
            popActionScreens: PopActionScreensType.all,
            bottomScreenMargin: 0.0,
            onWillPop: (context) async {
              return false;
            },
            selectedTabScreenContext: (context) {},
            hideNavigationBar: hideNavBar,
            decoration:
                NavBarDecoration(borderRadius: BorderRadius.circular(0.0)),
            popAllScreensOnTapOfSelectedTab: true,
            itemAnimationProperties: ItemAnimationProperties(
                duration: Duration(milliseconds: 400), curve: Curves.ease),
            screenTransitionAnimation: ScreenTransitionAnimation(
                animateTabTransition: true,
                curve: Curves.ease,
                duration: Duration(milliseconds: 200)),
            navBarStyle: NavBarStyle
                .style15, // Choose the nav bar style with this property
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
