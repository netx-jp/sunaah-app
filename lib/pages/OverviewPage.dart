import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class OverviewPage extends StatefulWidget {
  final data;

  OverviewPage({Key key, this.data}) : super(key: key);

  @override
  OverviewPageState createState() => OverviewPageState();
}

class OverviewPageState extends State<OverviewPage> {

  CarouselController carouselController = new CarouselController();
  int currentSlide = 0;
  String buttonText = "Next";
  List<int> sliders = [1, 2, 3, 4, 5, 6];
  List<String> defaultReligionIconLocations = [
    'assets/images/islam_white.png',
    'assets/images/buddha_white.png',
    'assets/images/cristian_white.png',
    'assets/images/hindu_white.png'

  ];
  List<String> transparentReligionIconLocations = [
    'assets/images/islam_blue.png',
    'assets/images/buddha_white.png',
    'assets/images/cristian_white.png',
    'assets/images/hindu_white.png'
  ];
  List<String> blueReligionIconLocations = [
    'assets/images/islam_blue.png',
    'assets/images/buddha_blue.png',
    'assets/images/cristian_blue.png',
    'assets/images/hindu_blue.png'
  ];

  bool needToFlipBg = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            Transform(
              alignment: Alignment.center,
              transform: needToFlipBg ? Matrix4.rotationY(math.pi) : Matrix4.rotationX(0 * math.pi / 2),
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/overview_bg_right.png',),
                    fit: BoxFit.cover
                  )
                ),
              ),
            ),
            Column(
              children: <Widget>[
                Expanded(
                    child: Container(
                      child: CarouselSlider(
                          carouselController: carouselController,
                          items: [
                            Container(
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Welcome to",
                                        style: TextStyle(fontSize: 20),
                                      ),
                                      Text(
                                        "Sunaah",
                                        style: TextStyle(
                                            fontSize: 40,
                                            fontWeight: FontWeight.bold
                                        ),
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(top: 10),
                                          height: 300.0,
                                          child: Image.asset('assets/images/mobile_img.png')
                                      )
                                    ]
                                )
                            ),
                            Container(
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Be Respect",
                                        style: TextStyle(
                                            fontSize: 40,
                                            fontWeight: FontWeight.bold
                                        ),
                                      ),
                                      Text(
                                        "to Religion, Get Always HALAL\nProduct",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 20),
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(top: 15),
                                          height: 250.0,
                                          child: Image.asset('assets/images/bread.png')
                                      )
                                    ]
                                )
                            ),
                            Container(
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      "choose the right",
                                      style: TextStyle(fontSize: 20),
                                    ),
                                    Text(
                                      "Bento",
                                      style: TextStyle(
                                          fontSize: 40,
                                          fontWeight: FontWeight.bold
                                      ),
                                    ),
                                    Container(
                                        margin: EdgeInsets.only(top: 15),
                                        height: 250.0,
                                        child: Image.asset('assets/images/bento.png')
                                    )
                                  ]
                              ),
                            ),
                            Container(
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      "Scan Before",
                                      style: TextStyle(fontSize: 20),
                                    ),
                                    Text(
                                      "Purchasing",
                                      style: TextStyle(
                                          fontSize: 40,
                                          fontWeight: FontWeight.bold
                                      ),
                                    ),
                                    Container(
                                        margin: EdgeInsets.only(top: 30),
                                        height: 250.0,
                                        child: Image.asset(
                                            'assets/images/qr_code_img.jpg'
                                        )
                                    )
                                  ]
                              ),
                            ),
                            Container(
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        "We have checked",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold
                                        ),
                                      ),
                                      Text(
                                        "For HALAL",
                                        style: TextStyle(
                                            fontSize: 40,
                                            fontWeight: FontWeight.bold
                                        ),
                                      ),
                                      Text(
                                        "Ingredient Products So You Don't need\nto worry",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.normal
                                        ),
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(top: 30),
                                          height: 350.0,
                                          width: double.maxFinite,
                                          child: Image.asset('assets/images/cups.png')
                                      )
                                    ]
                                )
                            ),
                            Container(
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Select Your",
                                        style: TextStyle(
                                            fontSize: 20, fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        "Religion",
                                        style: TextStyle(
                                            fontSize: 40, fontWeight: FontWeight.bold),
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(top: 10),
                                          child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                                  children: [
                                                    InkWell(
                                                      onTap: () {
                                                        setState(() {
                                                          transparentReligionIconLocations[0] = blueReligionIconLocations[0];
                                                          transparentReligionIconLocations[1] = defaultReligionIconLocations[1];
                                                          transparentReligionIconLocations[2] = defaultReligionIconLocations[2];
                                                          transparentReligionIconLocations[3] = defaultReligionIconLocations[3];
                                                        });
                                                      },
                                                      child: Container(
                                                        margin: EdgeInsets.all(5),
                                                        height: 100,
                                                        width: 100,
                                                        decoration: BoxDecoration(
                                                            image: DecorationImage(
                                                              image: AssetImage(transparentReligionIconLocations[0]),
                                                              fit: BoxFit.cover,
                                                            )
                                                        ),
                                                      ),
                                                    ),
                                                    InkWell(
                                                      onTap: () {
                                                        setState(() {
                                                          transparentReligionIconLocations[0] = defaultReligionIconLocations[0];
                                                          transparentReligionIconLocations[1] = blueReligionIconLocations[1];
                                                          transparentReligionIconLocations[2] = defaultReligionIconLocations[2];
                                                          transparentReligionIconLocations[3] = defaultReligionIconLocations[3];
                                                        });
                                                      },
                                                      child: Container(
                                                        height: 100,
                                                        width: 100,
                                                        margin: EdgeInsets.all(5),
                                                        decoration: BoxDecoration(
                                                            image: DecorationImage(
                                                              image: AssetImage(transparentReligionIconLocations[1]),
                                                              fit: BoxFit.cover,
                                                            )
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                    crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                    children: [
                                                      InkWell(
                                                        onTap: () {
                                                          setState(() {
                                                            transparentReligionIconLocations[0] = defaultReligionIconLocations[0];
                                                            transparentReligionIconLocations[1] = defaultReligionIconLocations[1];
                                                            transparentReligionIconLocations[2] = blueReligionIconLocations[2];
                                                            transparentReligionIconLocations[3] = defaultReligionIconLocations[3];
                                                          });
                                                        },
                                                        child: Container(
                                                          height: 100,
                                                          width: 100,
                                                          margin: EdgeInsets.all(5),
                                                          decoration: BoxDecoration(
                                                              image: DecorationImage(
                                                                image: AssetImage(transparentReligionIconLocations[2]),
                                                                fit: BoxFit.cover,
                                                              )
                                                          ),
                                                        ),
                                                      ),
                                                      InkWell(
                                                        onTap: () {
                                                          setState(() {
                                                            transparentReligionIconLocations[0] = defaultReligionIconLocations[0];
                                                            transparentReligionIconLocations[1] = defaultReligionIconLocations[1];
                                                            transparentReligionIconLocations[2] = defaultReligionIconLocations[2];
                                                            transparentReligionIconLocations[3] = blueReligionIconLocations[3];
                                                          });
                                                        },
                                                        child: Container(
                                                          height: 100,
                                                          width: 100,
                                                          margin: EdgeInsets.all(5),
                                                          decoration: BoxDecoration(
                                                              image: DecorationImage(
                                                                image: AssetImage(transparentReligionIconLocations[3]),
                                                                fit: BoxFit.cover,
                                                              )
                                                          ),
                                                        ),
                                                      )
                                                    ]
                                                )
                                              ]
                                          )
                                      )
                                    ]
                                )
                            )
                          ],
                          options: CarouselOptions(
                              height: double.maxFinite,
                              viewportFraction: 1.0,
                              enlargeCenterPage: false,
                              onPageChanged: (index, reason) {
                                if (index == (sliders.length -1)) {
                                  setState(() {
                                    buttonText = "Get Started";
                                  });
                                } else {
                                  setState(() {
                                    buttonText = "Next";
                                  });
                                }

                                if(index == 1 || index == 3){
                                  setState(() {
                                    needToFlipBg = true;
                                  });
                                }else{
                                  setState(() {
                                    needToFlipBg = false;
                                  });
                                }

                                setState(() {
                                  currentSlide = index;
                                });
                              }
                          )
                      ),
                    ),
                    flex: 4
                ),
                Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: sliders.map((slide) {
                              int index = sliders.indexOf(slide);
                              return Container(
                                width: 10.0,
                                height: 10.0,
                                margin: EdgeInsets.symmetric(
                                    vertical: 5.0, horizontal: 2.0),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: currentSlide == index
                                        ? Colors.blueAccent
                                        : Colors.black12
                                ),
                              );
                            }
                            ).toList(),
                          ),
                          InkWell(
                            onTap: () {
                              if(buttonText == "Get Started"){
                                Navigator.pushNamedAndRemoveUntil(context, "/dashboard", (r) => false);
                              }else {
                                carouselController.nextPage();
                              }
                            },
                            child: Container(
                              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                              width: 200.0,
                              height: 40.0,
                              decoration: BoxDecoration(
                                color: Colors.blueAccent,
                                borderRadius: new BorderRadius.circular(20.0),
                              ),
                              child: Center(
                                  child: Text(buttonText,
                                      style: TextStyle(fontSize: 18.0, color: Colors.white
                                      )
                                  )
                              ),
                            ),
                          )
                        ]
                    ),
                    flex: 1
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
