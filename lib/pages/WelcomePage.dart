import 'dart:async';
import 'package:flutter/material.dart';
import 'package:sunaah/utilities/FadeAnimation.dart';

class WelcomePage extends StatefulWidget {
  WelcomePage({Key key}) : super(key: key);

  @override
  WelcomePageState createState() => WelcomePageState();
}

class WelcomePageState extends State<WelcomePage>
    with SingleTickerProviderStateMixin {
  WelcomePageState({Key key});

  AnimationController animationController;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
  @override
  void initState() {
    super.initState();

    Timer(Duration(seconds: 7), () {
      Navigator.pushNamedAndRemoveUntil(context, "/overview", (r) => false);
    });
                         
    animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300)
    );
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: screenSize.height,
          width: screenSize.width,
          decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/welcome_bg.jpg'),
                fit: BoxFit.cover,
              )),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(),
                Container(
                  child: Column(
                    children: [
                      FadeAnimation(
                          1,
                          Container(
                              height: 100,
                              width: 200,
                              child: Image.asset('assets/images/logo.png'))),
                      Text(
                        "Scan for Halal Product",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 17,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                  child: Column(
                    children: [
                      FadeAnimation(
                          2,
                          Container(
                            width: 150,
                            padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                            child: Center(
                              child: Divider(
                                color: Colors.black,
                                thickness: 0.2,
                                height: 0.5,
                              ),
                            ),
                          )),
                      FadeAnimation(
                          3,
                          Text(
                            "Design & Developed by NETX",
                            style: TextStyle(color: Colors.black, fontSize: 15),
                          ))
                    ],
                  ),
                ),
              ]) /* add child content here */,
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    animationController.dispose();
  }
}
